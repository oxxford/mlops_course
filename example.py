import os

import pandas as pd


def sum(a: int, b: int) -> int:
    return a + b


if __name__ == "__main__":
    example = pd.DataFrame()
    os.listdir(".")

    print(sum(4, 5))

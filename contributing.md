Before contributing to this repository, please ensure that your code complies with the following standards:
- ruff liner (run ```ruff check FILENAME.py``` to verify)
- ruff formatter (run ```ruff format FILENAME.py``` to verify)
- mypy type guidelines (run ```mypy FILENAME.py``` to verify)
